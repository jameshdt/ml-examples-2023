Sir James Dyson has accused the government of having a "stupid" and "short-sighted" approach to the economy and business in the UK.
He said growth had "become a dirty word" during Rishi Sunak's premiership.
Writing in the Daily Telegraph, the Dyson founder urged the government to "incentivise private innovation and demonstrate its ambition for growth".
Michael Gove, levelling-up secretary, said the government was "firing on all cylinders" to help business.
A prominent supporter of Brexit with a fortune worth £23bn according to the Sunday Times Rich List, Sir James said the government believed it could "impose tax upon tax on companies in the belief that penalising the private sector is a free win at the ballot box".
"This is as short-sighted as it is stupid. In the global economy, companies will simply choose to transfer jobs and invest elsewhere," he warned.
The high-profile businessman's comments come after the government announced in the autumn £25bn worth of tax rises in an attempt to balance the books and restore credibility to the UK's finances after the economic fallout of the Liz Truss and Kwasi Kwarteng mini-budget.
One of the mini-budget polices reversed was a change to corporation tax, which is paid to the government by UK companies and foreign companies with UK offices. It is set to rise from 19% to 25% in April.
What is corporation tax and who has to pay it?
Sir James Dyson: From barrows to billions
UK businesses, as well as households, are also dealing with the highest rate of inflation for four decades, with energy bills driving price rises through supply chains.
Chaz Curry is one of thousands of business owners to close their doors in 2022.
He told the BBC he decided to shut Roots Cycleworks, a bicycle repair and hire and repair shop based in Exmouth, because of several factors including inflation, import issues, supplies of parts and customers' disposable income decreasing. He is set to start a new job next month.
