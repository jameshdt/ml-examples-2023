Using the torch on his mobile phone to guide him, Yaroslav Amosov climbs out of the darkness, up some dusty, broken stairs into the light.

Below is a basement, above are the remnants of a bombed-out home. Carefully cradled in Amosov's arms is a package.

The Ukrainian peels back a plastic bag, unwraps a towel and, with a smile, wipes some dust from the Bellator welterweight title belt.

Much of the house has been destroyed by Russian shelling following the country's invasion of Ukraine in February 2022, but champion Amosov's belt remarkably remains intact.

It is a moment of happiness and hope among the crumbled surroundings of a battle-scarred town.

"Before my mum left for safety she hid the belt in her house, which obviously got bombed and ruined along with everything else," Amosov, 29, told BBC Sport.

"When we liberated my city I was part of it, and in the next day or so I said 'hey, you know what, I'm going to go back to the ruins of my old house and see if I can find the belt'.

"I was really excited. It was very symbolic at the time of a different life I guess, and I was happy to find it. Now it's kind of become part of my history."

On 25 February, against Logan Storley at Bellator 291 in Dublin and live on BBC iPlayer, Amosov will defend his title for the first time since helping to defend his country against Russia's invasion.

With the war ongoing, leaving Ukraine was a difficult decision for Amosov, but the reasoning behind his return to the cage transcends competition.

"With enough pressure from my family and friends, I started to see what they were saying. Basically, return to MMA, go back to fighting and spread the word. Tell people what's been going on [in Ukraine]. This is the reason I came back."

This is his story of war.
Amosov was born in Irpin, a city on the outskirts of the Ukrainian capital, Kyiv.

He describes Irpin as a once-beautiful city, full of happiness and life.

This is where Amosov and his family were last February when Russia's president Vladimir Putin ordered his troops to launch a full-scale assault on Ukraine.

Irpin has suffered heavy damage in the attack, with hundreds of residents killed and thousands more uprooted from their homes.

"At first you're kind of in a state of shock. You don't know what's going on," said Amosov.

"The most important thing for us was to get our families to safety. Me and my friends, my parents, our wives, our elderly, our kids, dogs - anybody.

"The flow of people, traffic jams, it was horrible as you can imagine. Everyone is trying to get to safety. We finished packing up, got everybody in the cars and then we hit the road. I drove for 36 hours straight to get them to safety."

With his family safely away from the frontline, Amosov regained some clarity and made a potentially life-changing decision - he would drive back to Irpin to defend his city.

He did not know if he would see his family again.
