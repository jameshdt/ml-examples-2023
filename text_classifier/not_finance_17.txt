A high school student has stabbed a teacher to death in a school in the French town of Saint-Jean-de-Luz.
French government spokesman Olivier Véran confirmed Wednesday's attack and said the perpetrator was 16 years old.
Police attended Saint-Thomas d'Aquin school with the local prosecutor, where the student was arrested.
French newspaper Sud Ouest said the attacker entered the classroom while the teacher was giving a Spanish class and attacked her.
The teacher was in her 50s and died of cardiac arrest after emergency services arrived at the school, local media reported.
French TV station BFM said the attacker locked the classroom door and stabbed the teacher in her chest.
Local prosecutor Jerome Bourrier said that an investigation had been opened by local police for assassination and the suspect was in custody. He added that the suspect was not known to the police or the justice system.
The prosecutor will give a news conference on Thursday afternoon to give further details about the investigation.
France's Education Minister Pap Ndiaye called the attack "a tragedy of extreme gravity" and expressed his condolences.
"Today is a time of emotion and a time for solidarity," he said on a visit to the school. "The whole nation is present here to express its sorrow and emotion."
Local media reported the student might have been suffering from mental health issues. They said at this stage of the investigation there was no suggestion the incident was terror-related.
In a news conference, Mr Véran said the government would support educators across the country in the wake of the incident.
"I can hardly imagine the trauma that this represents," he said.
The school is a private, Catholic establishment near the centre of Saint-Jean-de-Luz, a well known French summer holiday location.
By lunchtime, students who had been told to remain in their classrooms were able to leave the school and many were collected by their parents.
