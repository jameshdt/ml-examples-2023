As I enter Ukraine, the streets are dark.

It's taken a day to get here and it feels surreal to finally be standing in a country at war.

Ten hours earlier I boarded a plane from London to Kraków in Poland. From there, it's a three hour drive to the Ukraine border.

I'm here with a team of journalists from Newsround to find out how children are doing, a year after Russia's invasion of Ukraine.

As we start our journey towards the border, it begins to snow.

Crossing into Ukraine is surprisingly straightforward. Our vehicle is parked up next to a white van painted with a Polish and Ukrainian flag. It's carrying clothes, bottles of water and other essential supplies.
Every vehicle is checked. I hand over my documents and we wait.

Less than 15 minutes later, the engine is on and we're on the move. The tyres of our car move forward slowly onto Ukrainian soil.

After months of planning, this is the moment I've been waiting for and my heart starts to beat faster.

Minutes later, the car stops. I step outside to record a quick video on my phone. The air is cold and it's so quiet.

I take a look around but there's not much to see in the dark.
We continue the drive to the city of Lviv in western Ukraine.

There are lots of houses set back from the main road. People are indoors, trying to stay warm - it's bitterly cold outside.

There are no lights on, instead I can see candles flicker in windows as we drive past a row of houses. There's a blackout, which means people in this part of the country are not getting electricity tonight.

The next morning, I'm up early and back in the car.

The journey to Kyiv, Ukraine's capital city is seven hours long.

Just like the UK, there are petrol stations and fast food restaurants all along the motorway, but there's something different that I've never seen before.

Buildings destroyed, apartment blocks badly damaged and warehouses burned to the ground.

It's all evidence of a war, something I have never seen with my own eyes.

This war - the biggest in Europe since World War Two - is now a year old. A year since Russian president Vladimir Putin ordered his armies to invade Ukraine.

Last year, 13-year-old Viola had to escape her home in the middle of the night after her village was taken over by Russian soldiers.
