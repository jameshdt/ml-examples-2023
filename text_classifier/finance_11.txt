My Money explores how people across the UK manage their spending in a typical week.
As prices rise, BBC News has been hearing about their ways of cutting costs and the financial choices they have to make.
Alannah is a 27-year-old blogger from north-east Scotland. She rents a two-bedroom house with her partner Jaimie, a delivery driver.
Alannah has Crohn's disease, a lifelong condition where parts of the digestive system become inflamed, and Ehlers-Danlos syndrome, a genetic illness that causes chronic pain. The couple's monthly income is about £2,000 from Jaimie's wages and their benefits. They have a baby son, Odin.
The house was freezing when I got up this morning. Outside it was about -4°C.
The heating was on, plus an electric heater in the living room. We're either in debt and warm, or freezing cold and still in debt. So I'd rather just have heat.
My television licence came out at £13.25, I hadn't expected it to come out today.
Another payment of £4.16 went to LayBuy, a buy now pay later company.
I stayed at home with my son today. Costcutter vouchers worth £10 arrived in the post from a local community fund group for families.
They're helpful for things like baby wipes but you don't get a lot from them as Costcutter is so expensive.
I'm in nearly £1,600 of energy debt and there's nothing I can do. That's on top of about £4,000 of other debt including credit card, rent and council tax arrears.
A charity called StepChange helped me pay off about half because it was about £8,000 in 2020. That felt amazing, but now I feel like I've taken 10 steps back with my energy debt.
There's a fund my energy supplier could use to help me pay it off, so we'll see if I qualify. I've also applied for the Warm Home Discount.
