There is now less sea-ice surrounding the Antarctic continent than at any time since we began using satellites to measure it in the late 1970s.
It is the southern hemisphere summer, when you'd expect less sea-ice, but this year is exceptional, according to the National Snow and Ice Data Center.
Winds and warmer air and water reduced coverage to just 1.91 million square km (737,000 sq miles) on 13 February.
What is more, the melt still has some way to go this summer.
Last year, the previous record-breaking minimum of 1.92 million sq km (741,000 sq miles) wasn't reached until 25 February.
Three of the last record-breaking years for low sea-ice have happened in the past seven years: 2017, 2022 and now 2023.
Research, cruise and fishing vessels are all reporting a similar picture as they make passage around the continent: most sectors are virtually ice-free.
Only the Weddell Sea remains dominated by frozen floes.
cientists consider the behaviour of Antarctic sea-ice to be a complicated phenomenon which cannot simply be ascribed to climate change.
Looking at the data from the last 40-odd years of available satellite data, the sea-ice extent shows great variability. A downward trend to smaller and smaller amounts of summer ice is only visible in the past few years.
Computer models had predicted that it would show long-term decline, much like we have seen in the Arctic, where summer sea-ice extent has been shrinking by 12-13% per decade as a result of global warming.
But the Antarctic hasn't behaved like that.
Data sources other than satellites allow us to look back at least as far as 1900.
These indicate Antarctic sea-ice was in a state of decline early in the last century, but then started to increase.
Recently it has shown great variability, with record satellite winter maximums and now record satellite summer minimums as well.
In winter, the floes can cover 18 million sq km (6.9 million sq miles), and more.
