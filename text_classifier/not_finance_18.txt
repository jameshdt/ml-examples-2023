A serial killer-obsessed woman who stabbed her on-off boyfriend to death with a dagger has been jailed for life.
Shaye Groves slit Frankie Fitzgerald's throat before plunging the blade into his chest 17 times at her home in Havant, Hampshire, in July 2022.
The 27-year-old, who was found guilty of murder after a trial, used information from true crime documentaries to plan her alibi.
She was jailed for a minimum of 23 years at Winchester Crown Court.
During her trial it was heard the pair shared a mutual interest in BDSM and a camera was set up in Groves' bedroom to record them having sex.
Police found framed pictures of notorious killers such as Myra Hindley and Peter Sutcliffe on her walls and books about criminals, including the notorious prisoner Charles Bronson.
She had knives, Viking axes and kept a Celtic dagger, used to kill Mr Fitzgerald, under her pillow or close to her bed.
Mr Fitzgerald, 25, who had two children, died at Groves' home in the early hours of 17July 2022.
Prosecutors said she acted out of jealousy and stabbed Mr Fitzgerald as he slept after she discovered he had been messaging who she mistakenly thought was a 13-year-old girl on social media.
The individual turned out to be 17, and had been blocked by Mr Fitzgerald.
Steven Perian KC, prosecuting, said Groves used knowledge gained from documentaries to portray herself to a friend as a victim of sexual violence.
She sent a friend videos of the pair having sex, edited to appear as rape - but the prosecution said the original footage showed it was actually consensual.
Mr Perian added: "The Crown say that the defendant - by reading about and watching murder documentaries - she was familiar with crime scenes, how to create a false narrative and how to set up a false alibi."
In a victim impact statement read in court, Mr Fitzgerald's mother Rosanne described him as "the shining light - a kind and beautiful person".
His father Barry's statement spoke of the family being "broken" by his killing.
"He had his life in front of him - this was snatched away. I'm not sure we'll ever get over this."
Passing sentence, judge The Hon Mr Justice Kerr said Groves was a "manipulative, possessive and jealous woman".
He said she and Mr Fitzgerald's relationship had a "dangerous dynamic" and was "marked by rough sex, cocaine and alcohol".
"You lost your temper and acted upon impulse. You loved the man you killed and killed the man you loved," he added.
