Darren Morgan from the ONS said there was a fall in health services with fewer operations and GP visits, while school attendance also dropped in the week before schools broke up for Christmas.
He explained that in terms of public services, the ONS measures things like teachers' wages, and how much investment is made in schools and the health services. "The services they provide are a really important part of the economy so we include it in our measurement," he said.
He also said that sporting activities, particularly football, were impacted because of the World Cup.
He said people were not "able to enjoy top-flight football due to the absence of Premier League football until Boxing Day, as the World Cup continued".
Mr Morgan added that rail and postal industries "had a poor month". "We certainly saw the impact of strikes as both fell heavily in December."
Strike action on trains caused disruption on the railways and on the roads in December. Postal workers also went on strike on a series of days in the run-up to Christmas.
The year-on-year comparison is not the same as adding up the quarterly growth figures.
It is a comparison of the full year with the full previous 12 months, which in 2021 included the lockdown at the beginning. When compared against that low base, the UK's economy was 4% bigger in 2022 than in 2021.
That was the biggest increase of all G7 nations for last year.
But the UK is also still the only G7 country where the economy is smaller than pre-pandemic levels.
