Adults under 50 in England have just over a week left to take up the NHS offer of a Covid booster jab.
It is the last opportunity for healthy 16-49-year-olds to get a top-up dose - if they have not yet had three jabs.
The vaccine can help protect against severe illness, even if you have caught Covid before.
Appointments are available at thousands of different sites, including GP practices and high street pharmacies, up to and including Sunday 12 February.
Anyone aged 16 or over - or an at-risk child aged 12 to 15 - who has had both primary jabs, qualify for this booster, which has been available for more than a year.
So far, 15,000 people have booked in for next week and there are still 391,000 appointments available, says NHS England.
How many people have had a winter Covid booster?
Can I get a free flu jab?
Amid the spread of the more contagious Omicron variant in December 2021, health officials announced a booster jab for those who had already had both of their Covid vaccinations. An already widespread vaccination programme was ramped up either side of Christmas that year to get as many people jabbed as possible.
That offer of a third jab for anyone remained available throughout last year. For the past few months, a further autumn booster has been available for over-50s and some other people with health issues.
NHS England has stressed that this fourth jab is not being rolled out to healthy under-50s. The new call is for those who never took up the offer of the third dose of vaccine to come forward now.
Professor Sir Chris Whitty, the chief medical officer for England, explained why the booster jab remained important. "We know that having the third vaccination, the booster, is a very important part of immunity to Covid and it provides additional protection."
But Prof Whitty said it would not be available indefinitely. "[It] is coming to an end on 12 February and I would encourage anybody who has not taken up the offer to do so before then."
After this date, the third jabs will only be offered to people considered to be at risk of serious illness, as recommended by the Joint Committee on Vaccination and Immunisation (JCVI). But people who have not received vaccinations at any stage are still being urged to come forward.
According to the latest figures, more than 151m Covid vaccine doses have been administered in the UK:
53.8m people have had a first dose
50.8m have had a second dose
40.4m have had a booster or third dose
The latest publicity campaign is designed to attract any of the 10 million people who opted to st two vaccinations.
The government has said it expects another autumn vaccination campaign later this year for older adults, as well as a potential spring campaign for the most vulnerable.
People with a severely weakened immune system were offered an additional third primary dose before being offered a booster this winter.
The NHS Covid vaccination programme will continue to run a smaller operation after 12 February, allowing those yet to come forward for first, second, or third doses if severely immunosuppressed, to book appointments.
Experts will also be keeping a close eye on new variants of Covid, in case more people need vaccinating in the future.
NHS Director of Vaccinations and Screening, Steve Russell, said: "There is just one week left of the autumn booster campaign and so if you are eligible for a booster but have yet to take up your latest dose, please do so before the end of next week.
"Whether you have had previous doses or a bout of Covid, we know that a booster is the best way to maintain protection against serious illness from Covid for yourself and your loved ones, so please do make the most of the offer while it is available and give yourself both protection and peace of mind for the year ahead."
