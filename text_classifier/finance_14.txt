The UK narrowly avoided falling into recession in 2022, new figures show, after the economy saw zero growth between October and December.
This is despite a sharp 0.5% fall in economic output during December, partly due to strike action, the Office for National Statistics (ONS) said.
Chancellor Jeremy Hunt said the figures showed "underlying resilience" but said "we are not out of the woods".
The Bank of England still expects the UK to enter recession this year.
But it thinks it will be shorter and less severe than previously forecast.
The Bank of England is the UK's central bank. The BBC included its view as it has a central role in managing the overall state of the economy.
One of the ways it does that is by changing interest rates. Recently, it has been raising rates in a bid to tackle the soaring cost of living.
Mr Hunt, who the BBC spoke to for the government's position, said that high inflation remains a problem and continues to cause "pain for families up and down the country".
Inflation - or the rate at which prices are rising - is slowing but at 10.5% remains close to a 40-year high.
