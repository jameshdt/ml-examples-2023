Two top Spanish transport officials have resigned over a botched order for new commuter trains that cost nearly €260m ($275m; £230m).
The trains could not fit into non-standard tunnels in the northern regions of Asturias and Cantabria.
The head of Spain's rail operator Renfe, Isaías Táboas, and the Secretary of State for Transport, Isabel Pardo de Vera, have now left their roles. 
The design fault was made public earlier this month. 
The Spanish government says the mistake was spotted early enough to avoid financial loss. However the region of Cantabria has demanded compensation. 
Renfe ordered the trains in 2020 but the following year manufacturer CAF realised that the dimensions it had been given for the trains were inaccurate and stopped construction.  
The rail network in northern Spain was built in the 19th Century and has tunnels under the mountainous landscape that do not match standard modern tunnel dimensions. 
The mistake means the trains will be delivered in 2026, two years late. 
Renfe and infrastructure operator Adif have launched a joint investigation to find out how the error could have happened. Earlier this month, Spain's transport ministry fired a Renfe manager and Adif's head of track technology over the blunder.
