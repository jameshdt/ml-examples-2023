The Sage Gateshead is to be renamed ahead of its 20th anniversary.
The landmark venue, designed by Sir Norman Foster's company, opened in 2004 and is famous for its curved roof.
It is named after one of its patrons, the software firm Sage Group, but a multi-million pound arena and convention centre, which will be called The Sage, is being built next door.
People can offer suggestions for the renaming of the existing building, which will be decided by the autumn.
Abigail Pogson, managing director of Sage Gateshead, said the rebrand would "make sure things are clear for audiences, artists, donors, trusts, foundations and other supporters as the new Sage arena is built".
She described the renaming as an opportunity to "turn a new page" before the building's anniversary in 2024.
"As we approach our twentieth year, this is an opportunity for us to affirm who we are, how we want to be known, and what we look like," Ms Pogson said.
Sage Gateshead, which is home to the Royal Northern Sinfonia, attracts around two million people each year and stages more than 400 performances annually.
Its distinctive curved roof is made from 3,000 stainless steel panels and 250 glass panels, and inside there are three main concert halls.
The new arena and convention centre received £10m from the Newcastle-based Sage Group and will open in 2024.
