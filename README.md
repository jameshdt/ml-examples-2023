# ML Examples 2023

There are two sample datasets in this repo.

* `finance_prop` - anonymised data indicating finance accepts and declines from a real motor finance lender
* `text_classified` - twenty text files on the topic of finance and economy, and twenty more on other topics

## Getting started

If you want to use [AutoGluon](https://auto.gluon.ai/stable/index.html) you can either
[install into your existing Python environment using pip](https://auto.gluon.ai/stable/install.html) or
get the [docker image](https://hub.docker.com/r/autogluon/autogluon).

## Finance prop data

There are two files:

* `ML_Train.csv` - 6655 acceptances and declines in ready-to-go format to use with [AutoGluon tabular predictor](https://auto.gluon.ai/stable/tutorials/tabular_prediction/index.html)
* `ML_Test.csv` - a smaller data set you can test your model with

How accuratebly can we predict the acceptances and declines? What can we infer if we run it against manually adjusted data,
for example changing the residential status etc?

## Text classification data

Some plain text files grabbed from the BBC website.

You can either follow the Autogluon workflow for [sentiment analysis](https://auto.gluon.ai/stable/tutorials/multimodal/text_prediction/beginner_text.html) (which is a similar task) or roll your own Bayesian classifier.

 
